/**
 * @file mypopen.c
 * mypopen_mypclose
 * Beispiel 2
 *
 * @author Balint Taschner <ic18b068@technikum-wien.at>
 * @author kerim Kahrimanovic <ic18b021@technikum-wien.at>
 * @author Fatma Jawad <ic18b020@technikum-wien.at>
 *
 * @date 2019.04.29
 *
 * @version 1
 */

#ifndef _MYPOPEN_H_
#define _MYPOPEN_H_
/*
 * -------------------------------------------------------------- includes --
 */

/*
### FB DM: all diese includes gehören ins Hauptfile und nicht ins Headerfile
außnahme für FILE
*/


#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
/*
 * --------------------------------------------------------------- defines --
 */

/*
### FB DM: auch das gehört imo ins C file, damit ich dort dann sehe, welchen
wert das Define hat
*/
#define READ_END 0
#define WRITE_END 1

/*
 * --------------------------------------------------- function prototypes --
 */
FILE *mypopen(const char *command, const char *modus);
int mypclose(FILE *stream);

void childAction(int pipeEnd[2], int unused_end, int used_end, const char *command, int fileno);
FILE *parentAction(int pipeEnd[2], int unused_end, int used_end, const char *type);

/* _MYPOPEN_H_ */
#endif
