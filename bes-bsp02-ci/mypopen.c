/**
 * @file mypopen.c
 * mypopen_mypclose
 * Beispiel 2
 *
 * @author Balint Taschner <ic18b068@technikum-wien.at>
 * @author kerim Kahrimanovic <ic18b021@technikum-wien.at>
 * @author Fatma Jawad <ic18b020@technikum-wien.at>
 *
 * @date 2019.04.29
 *
 * @version 1
 */

/*
 * --------------------------------------------------------------includes--
 */

/*
### FB DM: siehe Kommentar in Headerfile
*/

#include "mypopen.h"

/*
 * ------------------------------------------------------------- functions --
 */

/*
 * ### FB MJ: Fehler beim komplilieren:
 * 	newpopen.c: In function ‘mypclose’:
	newpopen.c:188:2: error: "/*" within comment [-Werror=comment]
	cc1: all warnings being treated as errors
 *
 * Testfälle: 9/30
 * Passed: 0, 11, 12, 23, 26, 27, 28, 29, 30
 */

/**
 *
 * \brief Implementierung von popen
 *             ... Aktuelle Pid
 *
 * \param command welches beim Kindsprocess ausgeführt werden soll
 * \param modus 'w' zum schreiben, 'r' zum lesen, Alle anderen Values fuehren zu einem Fehler
 *
 * \return FILE Pointer wenn erfolgreich sonst NULL
 *
 */
FILE *mypopen(const char *command, const char *modus){

	/*variablen für file descriptor*/
	int pipeEnd[2];

/**
### FB DM: diese variablen könnten global sein um zb bei parentAction() und childAction()
die Parameter zu vermeiden
 */	
	static FILE *stream = NULL;
	static pid_t childPID;

/**
### FB DM: errno setzten?
 */
	/*überprüfen ob Filepointer offen ist*/
	if(stream !=NULL){
		return NULL;
	}

/**
### FB DM: was ist wenn command ein leerzeichen ist?
 */
	/*überprüfen ob richtige mode mitgegeben sind, bzw ob commands mitgegeben wurden*/
	if(modus!=NULL){
		if ((strcmp(modus, "r") != 0 && strcmp(modus, "w") != 0) || command == NULL){
			errno = EAGAIN;
			return NULL;
		}
	}

	/*checken ob pipe offen schief läuft*/
	if(pipe(pipeEnd) == -1){
		errno = EINVAL;
		return NULL;
	}

	/* schauen nach kind process */
	childPID = fork();

	/*Child Process*/
	if(childPID == (pid_t) 0){

		/*liest command*/
		if (strcmp(modus, "r") == 0)
			childAction(pipeEnd, READ_END, WRITE_END, command, STDOUT_FILENO);

		/*schreibt command*/
		else
			childAction(pipeEnd, WRITE_END, READ_END, command, STDIN_FILENO);
	}

	/*Parent Process*/
	else if (childPID > (pid_t) 0){

		/*liest command*/
		if (strcmp(modus, "r") == 0){
			stream = parentAction(pipeEnd, READ_END, WRITE_END, modus);
			return stream;
		}

		/*schreibt command*/
		else{
			stream = parentAction(pipeEnd, WRITE_END, READ_END, modus);
			return stream;
		}
	}
	/*Falls fork fehlschlägt*/
	else{
		close (pipeEnd[0]);
		close (pipeEnd[1]);
		return NULL;
	}
	/*falls nichts klappt*/
	return NULL;
}
/**
 *
 * \brief verarbeitung von Child Process
 *
 * \param pipeEnd pipe öffnungen
 * \param unusedEnd die öffnung die wir nicht brauchen werden
 * \param usedEnd dieses öffnung verwenden wir
 * \param cammand was beim Kindsprocess ausgeführt werden soll
 * \param fileno zum prüfen mit pipe wichtig
 *
 * \return void
 *
 */
void childAction(int pipeEnd[2], int unusedEnd, int usedEnd, const char *command, int fileno){

/**
### FB DM: da nichts mit dem return gemacht wird > (void) close ..
 */
	/* schließen vom unnötigen pipe*/
	close (pipeEnd[unusedEnd]);

/**
### FB DM: was ist wenn bereits verlinkt? und return?
 */
	/*verlinken von pipe an STDOUT*/
	if (dup2(pipeEnd[usedEnd], fileno) == -1) {
		close (pipeEnd[usedEnd]);
		/*
		 * ## FB MJ: Returnwert angeben oder exit(EXIT_FAILURE)
		 */
		return;
	}
/**
### FB DM: da nichts mit dem return gemacht wird > (void) close ..
 */
	/*schließen von der anderen pipe Ende*/
	close(pipeEnd[usedEnd]);

/**
### FB DM: da nichts mit dem return gemacht wird > (void) excel ..
 */
	/*Befehl in shell ausführen*/
	execl("/bin/sh", "sh", "-c", command, (char *)NULL);

/**
### FB DM: return?
 */
	/*Wenn execl fehlschlägt wird diese zeile ausgeführt*/
	/*
	 * ## FB MJ: Returnwert angeben oder exit(EXIT_FAILURE)
	 */
	return;
/**
### FB DM: Beendigung des Kindprozess?
 */
}
/**
 *
 * \brief verarbeitung von Parent Process
 *
 * \param pipeEnd pipe öffnungen
 * \param unusedEnd die öffnung die wir nicht brauchen werden
 * \param usedEnd dieses öffnung verwenden wir
 * \param modus
 *
 * \return File
 *
 */
FILE *parentAction(int pipeEnd[2], int unusedEnd, int usedEnd, const char *modus){

	/*return wert defenieren und initialisieren*/
	FILE* fp_temp = NULL;

/**
### FB DM: da nichts mit dem return gemacht wird > (void) close ..
 */
	/* schließen vom unnötigen pipe*/
	close (pipeEnd[unusedEnd]);

	/*File Descriptor in Filepointer umwandeln*/
	if ((fp_temp = fdopen(pipeEnd[usedEnd], modus)) == NULL){

		/*Falls fdopen fehlgeschlagen schließen von verwendeten ressourcen*/
		close(pipeEnd[usedEnd]);
		return NULL;
	}

	/*zurück liefern vom convertierten File pointer*/
	return fp_temp;
}

int mypclose(FILE *stream){
	static FILE* fp = NULL;
	static pid_t childPID;
    pid_t pidWait = -1;
    int status = 0;

	/*checkt ob fp bereits offen ist, wenn nicht -> popen durchfuehren*/
    if (fp == NULL)   {
        errno = ECHILD;
        return -1;
    }

    if (fp != stream){
        errno = EINVAL;
        return -1;
    }

	/*EOF zeigt, dass bereits ein "stream" vorhanden ist und retourniert in diesem Fall -1*/
    if (fclose(stream) == EOF){
        fp = NULL;
        childPID = -1;
        errno = ECHILD;
        return -1;
    }
	
	/*errno erneuen indem 0 zugewiesen wird*/
    errno = 0;

	/*erlaubt es dem Kindsprozess zu terminieren*/
    while ((pidWait = waitpid(childPID, &status, 0)) != childPID){
        if (pidWait == -1){
            if (errno == EINTR){
                continue;
            }
        }

		/*nur fuer den fall das waidPID einen unerwarteten Wert retourniert*/
        fp = NULL;
        childPID= -1;
        errno = ECHILD;
        return -1;
    }

    fp = NULL;
    childPID=-1;

	/*wenn Status ungleich 0, alles ok, ansonsten retourniert er -1*/
    if (WIFEXITED(status) != 0){
        return WEXITSTATUS(status);
    }

    errno = ECHILD;
    return -1;
}

