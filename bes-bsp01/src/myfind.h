/**
 * @file myfind.h
 * @author Daniel Melichar (ic18b503@technikum-wien.at)
 * @brief Betriebssysteme Beispiel 01: myfind Headers
 * @version 0.1
 * @date 2019-03-04
 * @url https://code.heim.melichar.xyz/fhtw/bes-bsp01.git
 * 
 * @copyright Copyright (c) 2019
 * 
 */

/**/
/* TYPEDEFS, STRUCTS */
/**/

// The struct containing our parameters
typedef struct params_s {
  char *location;
  int help;
  int print;
  int ls;
  int nouser;
  char type;
  char *user;
  unsigned int userid;
  char *path;
  char *name;
  struct params_s *next;
} params_t;

/**/
/* HEADERS */
/**/
void do_file(const char *file_name, const params_t);
void do_dir(const char *dir_name, const params_t);
void print_usage(void);
