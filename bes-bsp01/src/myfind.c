/**
 * @file myfind.c
 * @author Daniel Melichar (ic18b503@technikum-wien.at)
 * @author Marco Judt (ic18b039@technikum-wien.at)
 * @author Florian Dienesch (ic18b509@technikum-wien.at)
 * @brief Betriebssysteme Beispiel 01: myfind
 * @version Abgabe
 * @date 2019-03-22
 * @url https://code.hc.melichar.xyz/fhtw/bes-bsp01.git
 *
 * @copyright Copyright (c) 2019
 *
 */

/**/
/* INCLUDES */
/**/
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <pwd.h>
#include <time.h>
#include <grp.h>
#include <stdio.h>
#include <fnmatch.h>
#include <libgen.h>
#include <limits.h>

/**/
/* DEFINES */
/**/

/**/
/* FUNCTIONS*/
/**/
void do_file(const char * file_name, const char * const * parms);
void do_dir(const char * dir_name, const char * const * parms);

int do_nouser(const uid_t uid);
void do_print (const char *file_name);
void do_ls(const char *file_name, const struct stat attribut);
int do_type(mode_t filemode, const char *param);
int do_name(const char *file_name, const char *param, int flag);
int do_user(const uid_t path, const char * param);

const char *do_filemask(const mode_t file_mode);
const char *do_time(time_t file_time);
void do_usage(void);

/**/
/* GLOBALS*/
/**/
const char *prog_name = "";

/* Main Function */

/**
* \brief Programstart of myfind
*
* This is where the program starts.
*
\param argc contains number of arguments
\param argv an array of const char pointer
\return 0 if successfully completed
\return 1 if failed
*/
int main(int argc, const char *argv[])
{
	prog_name = argv[0];

	if (argc <= 0)
	{
		fprintf(stderr, "%s: argc: %s\n", prog_name, strerror(errno));
		return EXIT_FAILURE;
	}
	else if(argc == 1)
	{
		DIR *d;
		struct dirent *dir;
		d = opendir(".");
		if (d)
		{
			while ((dir = readdir(d)) != NULL)
			{
				printf("%s\n", dir->d_name);
			}
			closedir(d);
		}
	}
	else if (argc > 1)
	{
		do_file(argv[1], argv);
	}

	return EXIT_SUCCESS;
}

/**
* \brief Error messege for usage of myfind
*
* Prints an messege on how to use the program.
*
\param void
\return void
*/
void do_usage(void)
{
	if (printf("usage:\n"
										"%s [ <location> ] [ <aktion> ]\n"
										"-user <name>|<uid>  entries belonging to a user\n"
										"-name <pattern>     entry names matching a pattern\n"
										"-type [bcdpfls]     entries of a specific type\n"
										"-print              print entries with paths\n"
										"-ls                 print entry details\n"
										"-nouser             entries not belonging to a user\n"
										"-path               entry paths (incl. names) matching a pattern\n", prog_name) < 0)
	{
		fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
		exit(1);
	}
	exit(1);
}

/**
* \brief Parse parameter and print files
*
Parse through all parameter and call respectively function. Prints files if necessary. Calls for every subdirectory do_dir.
*
\param file_name Full pathname to print
\param parms Committed arguments
\return void
*/
void do_file(const char * file_name, const char * const * parms)
{
	struct stat attribut;
	int i = 2; // Counter - starting at action, skips path
	int flag = 1; // for loop
	int printed = 0; // so it doesn't print twice
	int nopath = 0; // so it doesn't call do_dir with an action
	int isError = 0;

	// if no path committed
	if (strcmp(file_name, "-print") == 0 || file_name == NULL)
	{
		file_name = ".";
		nopath = 1;
		do_dir(file_name, parms);
	}
	else if (lstat(file_name, &attribut) != 0)
	{
		fprintf(stderr, "%s: lstat(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}

	do
	{
		// as long as there are still arguments left
		if (parms[i] != NULL)
		{
			// parameters with a single part
			if (strcmp(parms[i], "-print") == 0)
			{
				do_print(file_name);
				i++;
				printed = 1;
				continue;
			}
			else if (strcmp(parms[i], "-ls") == 0)
			{
				do_ls(file_name, attribut);
				i++;
				printed = 1;
				continue;
			}
			else if (strcmp(parms[i], "-nouser") == 0)
			{
				flag = do_nouser(attribut.st_uid);
				i++;
				printed = 1;
				// continue;
			}
			// paramters with a second part
			else if (strcmp(parms[i], "-type") == 0)
			{
				i++;
				if (parms[i] != NULL)
				{
					flag = do_type(attribut.st_mode, parms[i]);
				}
				else
				{
					fprintf(stderr, "%s: missing argument to '-type'\n", prog_name);
					exit(1);
				}
			}
			else if (strcmp(parms[i], "-name") == 0)
			{
				i++;
				if (parms[i] != NULL)
				{
					flag = do_name(basename((char*) file_name), parms[i], FNM_NOESCAPE);
				}
				else
				{
					fprintf(stderr, "%s: missing argument to '-name'\n", prog_name);
					exit(1);
				}
			}
			else if (strcmp(parms[i], "-path") == 0)
			{
				i++;
				if (parms[i] != NULL)
				{
					flag = do_name(file_name, parms[i], FNM_NOESCAPE);
				}
				else
				{
					fprintf(stderr, "%s: missing argument to '-path'\n", prog_name);
					exit(1);
				}
			}
			else if(strcmp(parms[i], "-user") == 0)
			{
				i++;
				if (parms[i] != NULL)
				{
					isError = do_user(attribut.st_uid, parms[i]);
				}
				else
				{
					fprintf(stderr, "%s: missing argument to '-user'\n", prog_name);
					exit(1);
				}
			}
			else
			{
				do_usage();
			}
			i++;
		}
		else
		{
			if (printed == 0 && isError == 0)
			{
				do_print(file_name);
			}

			flag = 0;
		}

	} while (flag == 1);

	if (nopath == 0)
	{
		if (S_ISDIR(attribut.st_mode))
		{
			do_dir(file_name, parms);
		}
	}
}

/**
* \brief Reads directory entries
*
Gets called for every subdirectory, opens it and reads its entries, calls do_file for every directory-entry with full pathname.
*
\param dir_name Pathname which is a directory
\param parms Committed arguments
\return void
*/
void do_dir(const char * dir_name, const char * const * parms)
{
	DIR *dir;
	struct dirent *entry;

	dir = opendir(dir_name);
	if (dir == NULL)
	{
		fprintf(stderr,"%s: opendir(%s): %s\n", prog_name, dir_name, strerror(errno));
		exit (1);
	}

	while ((entry = readdir(dir)) != NULL)
	{
		// skip '.' and '..'
		if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
		{
			continue;
		}

		// create an absolute path and call do_file with it
		char fullpath[strlen(dir_name) + strlen(entry->d_name) + 2];
		strcpy(fullpath, dir_name);
		if (fullpath[strlen(fullpath) - 1] != '/')
		{
			strcat(fullpath, "/");
		}
		strcat(fullpath, entry->d_name);
		do_file(fullpath, parms);
	}
	if (closedir(dir) != 0)
	{
		fprintf(stderr,"%s: closedir(%s): %s\n", prog_name, dir_name, strerror(errno));
		exit (1);
	}
}

/**
* \brief Standard output of filename
*
Prints the given filename with printf and errorhandling
*
\param file_name Filename or pathname to be printed
\return void
*/
void do_print (const char *file_name)
{
	if(printf("%s\n", file_name) < 0)
	{
		fprintf(stderr, "%s: printf(%s): %s\n", prog_name, file_name, strerror(errno));
		exit (1);
	}
}

/**
* \brief Checks if file has a owner
*
Checks with getpwuid if there is a uid in struct passwd.
*
\param uid UID of file
\return 1 if the file has no user
\return 0 if the file has a user
*/
int do_nouser(const uid_t uid)
{
	struct passwd *pwd;

	pwd = getpwuid(uid);
	if(pwd == NULL)
	{
		fprintf(stderr,"%s: getpwuid(): %s\n", prog_name, strerror(errno));
		return EXIT_FAILURE;
	}
	// nouser
	else if(pwd == 0)
	{
		return 1;
	}
	// user exists
	return 0;
}

/**
* \brief Prints formatted, detailed information of file
*
Prints number of inodes, number of blocks, permissions, number of links, owner, group, last modification time and directoryname. At symlinks the destination is added.
Distinguishes between block or character special files and regular files.
*
\param file_name Path of file to print
\param attribut Struct stat variable to get and print information about file
\return void
*/
void do_ls(const char *file_name, const struct stat attribut)
{
	struct passwd *pwd = NULL;
	struct group *grp = NULL;
 	char *owner = NULL;
	char *group = NULL;

	// Get uid
	errno = 0;
	pwd = getpwuid(attribut.st_uid);
	if (pwd == NULL)
	{
		fprintf(stderr, "%s: getpwuid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}
	if(errno == 0)
	{
		owner = pwd->pw_name;
	}
	else
	{
		fprintf(stderr, "%s: getpwuid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit (1);
	}

	// Get gid
	errno = 0;
	grp = getgrgid(attribut.st_gid);
	if (grp == NULL)
	{
		fprintf(stderr, "%s: getgrgid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}
	if(errno == 0)
	{
		group = grp->gr_name;
	}
	else
	{
		fprintf(stderr, "%s: getgrgid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}

	// Block or character special file
	if(S_ISBLK(attribut.st_mode) || S_ISCHR(attribut.st_mode))
	{
		if(printf("%6ld %4ld %s %3lu %-8s %-8s %s %s", attribut.st_ino, attribut.st_blocks / 2, /* Da die Blockgroesse bei ls 512 (statt 1024) ist */ do_filemask(attribut.st_mode),
															attribut.st_nlink, owner, group, do_time(attribut.st_mtime), file_name) < 0)
		{
			fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
			exit (1);
		}
	}
	// Regular files
	else
	{
		if(printf("%6ld %4ld %s %3lu %-8s %-8s %8ld %s %s", attribut.st_ino, attribut.st_blocks / 2, /* Da die Blockgroesse bei ls 512 (statt 1024) ist */ do_filemask(attribut.st_mode),
															attribut.st_nlink, owner, group, attribut.st_size, do_time(attribut.st_mtime), file_name) < 0)
		{
			fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
			exit (1);
		}
	}

	// Symlinks
	if (S_ISLNK(attribut.st_mode))
	{
		char symlink[attribut.st_size + 1];
		int length = readlink(file_name, symlink, attribut.st_size);

		if (length == -1)
		{
			fprintf(stderr, "%s: readlink(%s): %s\n", prog_name, file_name, strerror(errno));
			exit (1);
		}
		else
		{
			symlink[length] = '\0';
			if(printf(" -> %s", symlink) < 0)
			{
				fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
				exit (1);
			}
		}
	}

	if(printf("\n") < 0)
	{
		fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
		exit (1);
	}
}

/**
* \brief Converts time into string
*
Converts the timestamp from a file with localtime and strftime into a string.
*
\param file_time Timestamp of the file received with lstat
\return A formatted string with the date of the file.
*/
const char *do_time(time_t file_time)
{
	struct tm *time = NULL;
	static char date[30];

	time = localtime(&file_time);
	if(time == NULL)
	{
		fprintf(stderr, "%s: localtime(): %s\n", prog_name, strerror(errno));
		exit (1);
	}
	else
	{
		if(strftime(date, sizeof(date), "%b %e %H:%M", time) == 0)
		{
			fprintf(stderr, "%s: strftime(): %s\n", prog_name, strerror(errno));
			exit (1);
		}
	}

	return date;
}

/**
* \brief Creates the permission pattern
*
Getting filetype and permissions from lstat and checking on respectively filetypes and permissions and create a string with the file information.
*
\param file_mode Filemode of the file from lstat
\return Permission pattern and information of the file as a string.
*/
const char *do_filemask(const mode_t file_mode)
{
	static char mode[11];
	strcpy(mode, "----------");

	// filetype information
	switch (file_mode & S_IFMT)
	{
		case S_IFREG: break;
		case S_IFDIR: mode[0] = 'd'; break;
		case S_IFLNK: mode[0] = 'l'; break;
		case S_IFBLK: mode[0] = 'b'; break;
		case S_IFCHR: mode[0] = 'c'; break;
		case S_IFIFO: mode[0] = 'p'; break;
		case S_IFSOCK: mode[0] = 's'; break;
		default: fprintf(stderr, "Filemode error\n");
	}

	// owner permissions
	if (((file_mode & S_IRWXU) & S_IRUSR) != 0)
	{
		mode[1] = 'r';
	}
	if (((file_mode & S_IRWXU) & S_IWUSR) != 0)
	{
		mode[2] = 'w';
	}
	if (((file_mode & S_IRWXU) & S_IXUSR) != 0)
	{
		mode[3] = 'x';
	}

	// group permissions
	if (((file_mode & S_IRWXG) & S_IRGRP) != 0)
	{
		mode[4] = 'r';
	}
	if (((file_mode & S_IRWXG) & S_IWGRP) != 0)
	{
		mode[5] = 'w';
	}
	if (((file_mode & S_IRWXG) & S_IXGRP) != 0)
	{
		mode[6] = 'x';
	}

	// other permissions
	if (((file_mode & S_IRWXO) & S_IROTH) != 0)
	{
		mode[7] = 'r';
	}
	if (((file_mode & S_IRWXO) & S_IWOTH) != 0)
	{
		mode[8] = 'w';
	}
	if (((file_mode & S_IRWXO) & S_IXOTH) != 0)
	{
		mode[9] = 'x';
	}

	return mode;
}

/**
* \brief Checks the file on respectively filetype
*
Getting filetype from lstat and checking on respectively filetypes. If matched flag is set to 1.
*
\param filemode Filemode of the file from lstat.
\param param The filetype committed as argument in argv[3].
\return flag is 1 if a matching filetype was found.
\return flag is 0 if no matching filetype was found.
*/
int do_type(mode_t filemode, const char *param)
{
	const char *type = param;
	int flag = 0;

	if(strlen(param) > 1)
	{
		do_usage();
		exit(1);
	}

	if (strcmp(type, "b") == 0)
	{
		flag = S_ISBLK(filemode); // block special file
	}
	else if (strcmp(type, "c") == 0)
	{
		flag = S_ISCHR(filemode); // character special file
	}
	else if (strcmp(type, "d") == 0)
	{
		flag = S_ISDIR(filemode); // directory
	}
	else if (strcmp(type, "p") == 0)
	{
		flag = S_ISFIFO(filemode); // pipe
	}
	else if (strcmp(type, "f") == 0)
	{
		flag = S_ISREG(filemode); // regular file
	}
	else if (strcmp(type, "l") == 0)
	{
		flag = S_ISLNK(filemode); // symlink
	}
	else if (strcmp(type, "s") == 0)
	{
		flag = S_ISSOCK(filemode); // socket
	}
	else
	{
		do_usage();
	}

	return flag;
}

/**
* \brief Checks a name with a given pattern
*
Compares a given name if it's equal to a pattern with fnmatch.
*
\param file_name The basename of the filename to be checked
\param param The committed name from argv[3]
\param flag The flags for fnmatch
\return 1 if the name and pattern are matching
\retrun 0 if the name and pattern are not matching
*/
int do_name(const char *file_name, const char *param, int flag)
{
	// globbing
	int ret = fnmatch(file_name, param, flag);

	// matching name
	if(ret == 0)
	{
		return 1;
	}
	else if(ret != FNM_NOMATCH)
	{
		fprintf(stderr, "%s: fnmatch(): %s\n", prog_name, strerror(errno));
		exit (1);
	}

	// no matching name
	return 0;
}

/**
* \brief Checks if the file has the comitted owner
*
Searches with library function getpwnam for the username and checks if the given user/uid matches with the file.
*
\param path uid from the owner given from lstat
\param param The owner to be checked given in argv[3]
\return 1 if the files owner is not the given param
\retrun 0 if the files owner matches with the given param
*/
int do_user(const uid_t path, const char * param)
{
	struct passwd *pwd = NULL;

	// get passwd entry for username
	pwd = getpwnam(param);

	// if username not found try as userid
	if(pwd == 0)
	{
		char *endptr;
		errno = 0;
		long userID = strtol(param, &endptr, 10);

		if ((userID == LONG_MAX || userID == LONG_MIN) && errno == ERANGE)
		{
			printf("Out of range!\n");
			return 1;
		}
		if (endptr == param)
		{
			printf("Nothing parsed from the string!\n");
			exit(1);
		}
		else
		{
			pwd = getpwuid(userID);
		}
	}

	if (pwd == 0)
	{
		return 1;
	}

	// user found
	if (path == pwd->pw_uid)
	{
		return 0;
	}

	// no user found
	return 1;
}
