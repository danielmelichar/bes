/**
 * @file myfind.c
 * @author Daniel Melichar (ic18b503@technikum-wien.at)
 * @author Marco Judt (ic18b039@technikum-wien.at)
 * @author Florian Dienesch (ic18b509@technikum-wien.at)
 * @brief Betriebssysteme Beispiel 01: myfind
 * @version 1.0
 * @date 2019-03-21
 * @url https://code.hc.melichar.xyz/fhtw/bes-bsp01.git
 *
 * @copyright Copyright (c) 2019
 *
 */


/**/
/* INCLUDES */
/**/
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <pwd.h>
#include <time.h>
#include <grp.h>
#include <stdio.h>

/**/
/* DEFINES */
/**/

/**/
/* FUNCTIONS*/
/**/
void do_file(const char * file_name, const char * const * parms);
void do_dir(const char * dir_name, const char * const * parms);

int do_nouser(const uid_t uid);
void do_print (const char *file_name);
void do_print_ls(const char *file_name, const struct stat attribut);
int do_type(mode_t filemode, const char *param);

void do_usage(void);
const char *do_filemask(const mode_t file_mode);
const char *do_time(time_t file_time);

/**/
/* GLOBALS*/
/**/
const char *prog_name = "";

/* Main Function */

/**
* \brief Programstart of myfind
*
* This is where the program starts.
*
\param argc contains number of arguments
\param argv an array of const char pointer
\return 0 if successfully completed
\return 1 if failed
*/
int main(int argc, const char *argv[])
{
	prog_name = argv[0];

	if (argc <= 0)
	{
		fprintf(stderr, "%s: argc: %s\n", prog_name, strerror(errno));
		return EXIT_FAILURE;
	}
	else if(argc == 1)
	{
		DIR *d;
		struct dirent *dir;
		d = opendir(".");
		if (d)
		{
			while ((dir = readdir(d)) != NULL)
			{
				printf("%s\n", dir->d_name);
			}
			closedir(d);
		}
	}
	else if (argc > 1)
	{
		do_file(argv[1], argv);
	}

	return EXIT_SUCCESS;
}

/**
* \brief Error messege for usage of myfind
*
* Prints an messege on how to use the program.
*
\param void
\return void
*/
void do_usage(void)
{
	if (printf("usage:\n"
										"%s [ <location> ] [ <aktion> ]\n"
										"-user <name>|<uid>  entries belonging to a user\n"
										"-name <pattern>     entry names matching a pattern\n"
										"-type [bcdpfls]     entries of a specific type\n"
										"-print              print entries with paths\n"
										"-ls                 print entry details\n"
										"-nouser             entries not belonging to a user\n"
										"-path               entry paths (incl. names) matching a pattern\n", prog_name) < 0)
	{
		fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
		exit(1);
	}
	exit(EXIT_SUCCESS);
}

/**
* \brief Parse parameter and print files
*
Parse through all parameter and call respectively function. Prints files if necessary. Calls for every subdirectory do_dir.
*
\param file_name Full pathname to print
\param parms Committed arguments
\return void
*/
void do_file(const char * file_name, const char * const * parms)
{
	struct stat attribut;
	int i = 2; // Counter - starting at action, skips path
	int flag = 1; // for loop
	int printed = 0; // so it doesn't print twice
	int nopath = 0; // so it doesn't call do_dir with an action

	// if no path committed
	if (strcmp(file_name, "-print") == 0)
	{
		file_name = ".";
		nopath = 1;
		do_dir(file_name, parms);
	}
	else if (lstat(file_name, &attribut) != 0)
	{
		fprintf(stderr, "%s: lstat(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}

	do
	{
		// as long as there are still arguments left
		if (parms[i] != NULL)
		{
			// parameters with a single part
			if (strcmp(parms[i], "-print") == 0)
			{
				do_print(file_name);
				i++;
				printed = 1;
				continue;
			}
			else if (strcmp(parms[i], "-ls") == 0)
			{
				do_print_ls(file_name, attribut);
				i++;
				printed = 1;
				continue;
			}
			else if (strcmp(parms[i], "-nouser") == 0)
			{
				flag = do_nouser(attribut.st_uid);
				i++;
				printed = 1;
				continue;
			}
			// paramters with a second part
			else if (strcmp(parms[i], "-type") == 0)
			{
				i++;
				if (parms[i] != NULL)
				{
					flag = do_type(attribut.st_mode, parms[i]);
				}
				else
				{
					fprintf(stderr, "%s: missing argument to '-type'\n", prog_name);
					exit(1);
				}
			}
			else
			{
				do_usage();
			}
			i++;
		}
		else
		{
			if (printed == 0)
			{
				do_print(file_name);
			}

			flag = 0;
		}

	} while (flag == 1);

	if (nopath == 0)
	{
		if (S_ISDIR(attribut.st_mode))
		{
			do_dir(file_name, parms);
		}
	}
}

/**
* \brief Reads directory entries
*
Gets called for every subdirectory, opens it and reads its entries, calls do_file for every directory-entry with full pathname.
*
\param dir_name Pathname which is a directory
\param parms Committed arguments
\return void
*/
void do_dir(const char * dir_name, const char * const * parms)
{
	DIR *dir;
	struct dirent *entry;

	dir = opendir(dir_name);
	if (dir == NULL)
	{
		fprintf(stderr,"%s: opendir(%s): %s\n", prog_name, dir_name, strerror(errno));
		exit (1);
	}

	while ((entry = readdir(dir)) != NULL)
	{
		// skip '.' and '..'
		if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
		{
			continue;
		}

		// create an absolute path and call do_file with it
		char fullpath[strlen(dir_name) + strlen(entry->d_name) + 2];
		strcpy(fullpath, dir_name);
		if (fullpath[strlen(fullpath) - 1] != '/')
		{
			strcat(fullpath, "/");
		}
		strcat(fullpath, entry->d_name);
		do_file(fullpath, parms);
	}
	if (closedir(dir) != 0)
	{
		fprintf(stderr,"%s: closedir(%s): %s\n", prog_name, dir_name, strerror(errno));
		exit (1);
	}
}

/**
* \brief Standard output of filename
*
Prints the given filename with printf and errorhandling
*
\param file_name Filename or pathname to be printed
\return void
*/
void do_print (const char *file_name)
{
	if(printf("%s\n", file_name) < 0)
	{
		fprintf(stderr, "%s: printf(%s): %s\n", prog_name, file_name, strerror(errno));
		exit (1);
	}
}

/**
* \brief Checks if file has a owner
*
Checks with getpwuid if there is a uid in struct passwd.
*
\param uid UID of file
\return 1 if the file has no user
\return 0 if the file has a user
*/
int do_nouser(const uid_t uid)
{
	struct passwd *pwd;

	errno = 0;
	pwd = getpwuid(uid);
	if(errno != 0)
	{
		fprintf(stderr,"%s: getpwuid(): %s\n", prog_name, strerror(errno));
		return EXIT_FAILURE;
	}
	// nouser
	else if(pwd == 0)
	{
		return 1;
	}
	// user exists
	return 0;
}

void do_print_ls(const char *file_name, const struct stat attribut)
{
	struct passwd *pwd = NULL;
	struct group *grp = NULL;
 	char *owner = NULL;
	char *group = NULL;

	// Get uid
	errno = 0;
	pwd = getpwuid(attribut.st_uid);
	if (pwd == NULL)
	{
		fprintf(stderr, "%s: getpwuid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}
	if(errno == 0)
	{
		owner = pwd->pw_name;
	}
	else
	{
		fprintf(stderr, "%s: getpwuid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit (1);
	}

	// Get gid
	errno = 0;
	grp = getgrgid(attribut.st_gid);
	if (grp == NULL)
	{
		fprintf(stderr, "%s: getgrgid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}
	if(errno == 0)
	{
		group = grp->gr_name;
	}
	else
	{
		fprintf(stderr, "%s: getgrgid(%s): %s\n", prog_name, file_name, strerror(errno));
		exit(1);
	}

	// Block or character special file
	if(S_ISBLK(attribut.st_mode) || S_ISCHR(attribut.st_mode))
	{
		if(printf("%6ld %4ld %s %3lu %-8s %-8s %s %s", attribut.st_ino, attribut.st_blocks / 2, /* Da die Blockgroesse bei ls 512 (statt 1024) ist */ do_filemask(attribut.st_mode),
															attribut.st_nlink, owner, group, do_time(attribut.st_mtime), file_name) < 0)
		{
			fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
			exit (1);
		}
	}
	// Regular files
	else
	{
		if(printf("%6ld %4ld %s %3lu %-8s %-8s %8ld %s %s", attribut.st_ino, attribut.st_blocks / 2, /* Da die Blockgroesse bei ls 512 (statt 1024) ist */ do_filemask(attribut.st_mode),
															attribut.st_nlink, owner, group, attribut.st_size, do_time(attribut.st_mtime), file_name) < 0)
		{
			fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
			exit (1);
		}
	}

	// Symlinks
	if (S_ISLNK(attribut.st_mode))
	{
		char symlink[attribut.st_size + 1];
		int length = readlink(file_name, symlink, attribut.st_size);

		if (length == -1)
		{
			fprintf(stderr, "%s: readlink(%s): %s\n", prog_name, file_name, strerror(errno));
			exit (1);
		}
		else
		{
			symlink[length] = '\0';
			if(printf(" -> %s", symlink) < 0)
			{
				fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
				exit (1);
			}
		}
	}

	if(printf("\n") < 0)
	{
		fprintf(stderr, "%s: printf(): %s\n", prog_name, strerror(errno));
		exit (1);
	}
}

const char *do_time(time_t file_time)
{
	struct tm *time = NULL;
	static char date[30];

	time = localtime(&file_time);
	if(time == NULL)
	{
		fprintf(stderr, "%s: localtime(): %s\n", prog_name, strerror(errno));
		exit (1);
	}
	else
	{
		if(strftime(date, sizeof(date), "%b %e %H:%M", time) == 0)
		{
			fprintf(stderr, "%s: strftime(): %s\n", prog_name, strerror(errno));
			exit (1);
		}
	}

	return date;
}

const char *do_filemask(const mode_t file_mode)
{
	static char mode[11];
	strcpy(mode, "----------");
	int exeflag = 0;

	// filetype information
	switch (file_mode & S_IFMT)
	{
		case S_IFREG: break;
		case S_IFDIR: mode[0] = 'd'; break;
		case S_IFLNK: mode[0] = 'l'; break;
		case S_IFBLK: mode[0] = 'b'; break;
		case S_IFCHR: mode[0] = 'c'; break;
		case S_IFIFO: mode[0] = 'p'; break;
		case S_IFSOCK: mode[0] = 's'; break;
		default: fprintf(stderr, "Filemode error\n");
	}

	// owner permissions
	if (((file_mode & S_IRWXU) & S_IRUSR) != 0)
	{
		mode[1] = 'r';
	}
	if (((file_mode & S_IRWXU) & S_IWUSR) != 0)
	{
		mode[2] = 'w';
	}
	exeflag = (file_mode & S_IRWXU) & S_IXUSR;
	if (exeflag != 0)
	{
		mode[3] = 'x';
	}

	// group permissions
	if (((file_mode & S_IRWXG) & S_IRGRP) != 0)
	{
		mode[4] = 'r';
	}
	if (((file_mode & S_IRWXG) & S_IWGRP) != 0)
	{
		mode[5] = 'w';
	}
	exeflag = (file_mode & S_IRWXG) & S_IXGRP;
	if (exeflag != 0)
	{
		mode[6] = 'x';
	}

	// other permissions
	if (((file_mode & S_IRWXO) & S_IROTH) != 0)
	{
		mode[7] = 'r';
	}
	if (((file_mode & S_IRWXO) & S_IWOTH) != 0)
	{
		mode[8] = 'w';
	}
	exeflag = (file_mode & S_IRWXO) & S_IXOTH;
	if (exeflag != 0)
	{
		mode[9] = 'x';
	}

	return mode;
}

int do_type(mode_t filemode, const char *param)
{
	const char *type = param;
	int flag = 0;

	if(strlen(param) > 1)
	{
		do_usage();
		exit(1);
	}

	if (strcmp(type, "b") == 0)
	{
		flag = S_ISBLK(filemode); // block special file
	}
	else if (strcmp(type, "c") == 0)
	{
		flag = S_ISCHR(filemode); // character special file
	}
	else if (strcmp(type, "d") == 0)
	{
		flag = S_ISDIR(filemode); // directory
	}
	else if (strcmp(type, "p") == 0)
	{
		flag = S_ISFIFO(filemode); // pipe
	}
	else if (strcmp(type, "f") == 0)
	{
		flag = S_ISREG(filemode); // regular file
	}
	else if (strcmp(type, "l") == 0)
	{
		flag = S_ISLNK(filemode); // symlink
	}
	else if (strcmp(type, "s") == 0)
	{
		flag = S_ISSOCK(filemode); // socket
	}
	else
	{
		do_usage();
	}

	return flag;
}
