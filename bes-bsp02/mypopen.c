/**
 * @file mypopen.c
 * Betriebsysteme Beispiel 2
 *
 * @author Daniel Melichar (ic18b503@fhtw)
 * @author Marco Judt (ic18b039@fhtw)
 * @author Florian Dienesch (ic18b509@fhtw)
 *
 * @git https://code.hc.melichar.xyz/fhtw/bes-bsp02
 * @date 2019
 * @version 1.0.0.0
 *
 * @ToDo mypopentest 04, 06, 19, 30(? never starts)
 */

/*
 * -------------------------------------------------------------- includes --
 */

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <string.h>
#include "mypopen.h"

/*
 * --------------------------------------------------------------- globals --
 */

static FILE *fsp = NULL; /*File pointer*/
static pid_t cpid = -1; /* Child process id */

/*
 * --------------------------------------------------------------- defines--
 */

#define READ 0
#define WRITE 1

/*
 * ------------------------------------------------------------- functions --
 */

/**
 * @brief The mypopen() function is opens a process by creating a pipe, forking, 
 * and invoking the shell. The type argument may specify only reading or writing, 
 * not both; the resulting stream is correspondingly read-only or write-only. 
 * The command argument is a pointer to a null-terminated string containing
 * a shell command line. This command is passed to /bin/sh using the -c flag
 *
 *
 * @param command the command to be executed
 * @param type either r for read or a char w for write
 * @return FILE*
 */
FILE *mypopen(const char *command, const char *type)
{
	int mode = 0; /*read or write type*/
	int pfd[2]; /* File descriptor for pipe (0 = read, 1, write) */

	/* Previous file has not been closed */
	if (fsp != NULL) {
		/* ressource temporarly unavailable, try again*/
		errno = EAGAIN;
		return NULL;
	}

	/* Verify command parameter*/
	if (command == NULL || command[0] == '\0') {
		/* Invalid argument */
		errno = EINVAL;
		return NULL;
	}

	/* Verify type parameter */
// ### FB BP: Hier kracht Test 4, weil type == NULL. [-2]
// ### FB BP: Hier fehlt ein == 0 - deshalb geht Test 6 schief. Und mode hat den falschen Wert. [-2]
	if (strcmp(type, "r")) {
		mode = READ;
// ### FB BP: Hier fehlt ein == 0 - deshalb geht Test 6 schief. Und mode hat (auch) den falschen Wert. [-0]
// ### FB BP: In der Folge kommt deshalb bei Test 6 auch der Output von "ls" auf der Shell raus ....
	} else if (strcmp(type, "w")) {
		mode = WRITE;
	} else {
		/* Invalid argument */
		// ToDo mypopentest04+06: never reached? also tried with condition
		errno = EINVAL;
		return NULL;
	}

	/* Create pipe */
	if (pipe(pfd) == -1) {
		/*Also set by system call, but just to be sure*/
		errno = EMFILE;
// #### FB BP: Warum nicht den - vom pipe() hinterlassenen - original-errno Wert verwenden?
		return NULL;
	}

	/* Create child and map standard file desc */
	switch (cpid = fork()) {
	case -1: /* Error during process creation */

		/* close pipe and abort */
		(void)close(pfd[READ]);
		(void)close(pfd[WRITE]);

		/*errno set by syscall*/
		return NULL;
		break;

	case 0: /* child process*/

		// ToDo mypopentest19
		// ToDo remove duplicate
		if (!mode) { /* WRITE */
			/* Child reads, close write*/
			(void)close(pfd[WRITE]);

			/* associate corrent stream */
			if (dup2(pfd[READ], STDIN_FILENO) == -1) {
				/* error, close opened stream */
				(void)close(pfd[READ]);
				_exit(EXIT_FAILURE);
			}

			/* close stream after association */
// ### FB BP: pfd[WRITE] ist bereits oben geschlossen worden. "close(pfd[READ])"?!
//            Test 19 geht deshalb schief.
			(void)close(pfd[WRITE]);

		} else { /* READ */
			/* Child writes, close read */
			(void)close(pfd[READ]);

			/* associate corrent stream */
			if (dup2(pfd[WRITE], STDOUT_FILENO) == -1) {
				/* error, close opened stream */
				(void)close(pfd[WRITE]);
				_exit(EXIT_FAILURE);
			}

			/* close stream after association */
// ### FB BP: pfd[READ] ist bereits oben geschlossen worden. "close(pfd[WRITE])"?!
//            Test 19 geht deshalb schief.
			(void)close(pfd[READ]);
		}

		/* Execute command */
		(void)execl("/bin/sh", "sh", "-c", command, (char *)NULL);
		/* error during execute */
		_exit(EXIT_FAILURE);

		break;

	default: /* parent process */

		/* Close file discriptor */
// ### FB BP: Das ist die echte Hackl�sung: eine Variable mit 0 bzw. 1 und f�r STDIN/STDOUT
//            und den Index in pfd verwenden!
		(void)close(pfd[mode]);

		/* get all  */
		fsp = fdopen(pfd[!mode], type);
		if (fsp == NULL) {
			(void)close(pfd[!mode]);
			break;
		}

		return fsp;
		break;
	}

	return NULL;
}

/**
 * @brief The return value from popen() must be closed with pclose()
 * It waits for the associated process to terminat and returns 
 * the exit status of the command
 *
 * @param stream the stream to be closed
 * @return int
 */
int mypclose(FILE *stream)
{
	int pStatus = 0; /* process status */
	pid_t pid = 0; /* child pid */

	/* mypopen has to be called before mypclose*/
	if (fsp == NULL) {
		errno = ECHILD;
		return -1;
	}

	/* This is not the process you are looking for - Ben Kenobi */
	if ((stream == NULL) || (stream != fsp)) {
		errno = EINVAL;
		return -1;
	}

	/* Close stream, reset if fail*/
	if (fclose(stream) == EOF) {
		/* reset all */
		fsp = NULL;
		cpid = -1;

		return -1;
	}

	/* Wait for child to be finished */
	pid = waitpid(cpid, &pStatus, 0);
	while (pid != cpid) {
		if (pid == -1 && errno != EINTR) {
			/* reset all */
			fsp = NULL;
			cpid = -1;

			return -1;
		}
/* ### FB BP: Und hier fehlt
		pid = waitpid(cpid, &pStatus, 0);
              damit die Schleife ein Chance auf Terminierung im Fehlerfall hat. [-2]
              Deshalb "h�ngt" (genauer: endlos-loopt) Test 30.
 */
	}

	fsp = NULL;
	cpid = 0;

	/* Evaluates child process termination status */
	if (WIFEXITED(pStatus)) {
		return WEXITSTATUS(pStatus);
	}

	errno = ECHILD;
	return -1;
}

/* eof */
