/**
 * @file mypopen.h
 * Betriebssysteme - Beispiel 2
 * mypopen() / mypclose()
 *
 * @author Daniel Melichar <ic18b503@fhtw>
 * @author Marco Judt <ic18b039@fhtw>
 * @author Florian Dienesch <ic18b509@fhtw>
 *
 * @date 2019
 */

/*
 * --------------------------------------- start of double inclusion check --
 */
#ifndef MYPOPEN_H
#define MYPOPEN_H

/*
 * -------------------------------------------------------------- includes --
 */
#include <stdio.h>	/* type FILE */


/*
 * ------------------------------------------------------------- functions --
 */
extern FILE *mypopen(const char *command, const char *type);
extern int mypclose(FILE *stream);

/*
 * ----------------------------------------- end of double inclusion check --
 */
#endif


/*
 * =================================================================== eof ==
 */
