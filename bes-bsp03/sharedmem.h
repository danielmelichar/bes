/**
 * @file sharedmem.h
 * Betriebsysteme Beispiel 3
 *
 * @author Daniel Melichar (ic18b503@technikum-wien.at)
 * @author Marco Judt (ic18b039@technikum-wien.at)
 * @author Florian Dienesch (ic18b509@technikum-wien.at)
 *
 * @git http://code.hc.melichar.xyz/fhtw/bes-bsp03.git
 * @date 2019
 * @version 1.0
 */

#ifndef SHAREDMEMORY_H
#define SHAREDMEMORY_H

/*
 * -------------------------------------------------------------- includes --
 */

#include <errno.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * --------------------------------------------------------------- globals --
 */

/*
 * --------------------------------------------------------------- defines--
 */

#define SEM_READ (1000 * getuid() + 0)
#define SEM_WRITE (1000 * getuid() + 1)
#define SEM_SHARED (1000 * getuid() + 2)
#define PERMISSIONS 0660

/*
 * -------------------------------------------------------------- struct --
 */

typedef struct ringbuffer
{
	sem_t sem_read;
	sem_t sem_write;
} ringbuffer;

/*
 * ------------------------------------------------------------- functions --
 */

void print_usage(char *error);
int create_semaphores(int sem_name);

/**
 * @brief initializes the semaphores and shared memory segment
 * @details 
 * 
 * @param size size of the shared memory segment
 */
void meminit(int size);

/**
 * @brief removes semaphores and marks shared memory segment for destruction
 */
void memrmv(void);

/**
 * @brief attaches the process to the shared memory segment
 */
void memattach(void);

/**
 * @brief detaches the process to the shared memory segment
 */
void memdetach(void);

/**
 * @brief reads from shared memory segment
 * @return the value read from the shared memory segment
 */
short memread(void);

/**
 * @brief writes to shared memory segment
 * 
 * @param elem element to be written
 */
void memwrite(const short elem);

#endif /* SHAREDMEMORY_H */
