/**
 * @file sharedmem.c
 * Betriebsysteme Beispiel 3
 *
 * @author Daniel Melichar (ic18b503@technikum-wien.at)
 * @author Marco Judt (ic18b039@technikum-wien.at)
 * @author Florian Dienesch (ic18b509@technikum-wien.at)
 *
 * @git http://code.hc.melichar.xyz/fhtw/bes-bsp03.git
 * @date 2019
 * @version 1.0
 */

/*
 * -------------------------------------------------------------- includes --
 */

#include "sharedmem.h"

/*
 * --------------------------------------------------------------- globals --
 */

const char * program_name;

/*
 * --------------------------------------------------------------- defines--
 */

/*
 * ------------------------------------------------------------- functions --
 */

void print_usage(char *error)
{
    fprintf(stderr, "Usage: %s: -m <ringbuffer elements>\n%s\n", program_name, error);
}

int create_semaphores(int sem_name)
{
	sem_t *sem;
	errno = 0;
	sem = sem_open("/sem_name", O_CREAT, PERMISSIONS, 0);
	if (sem == SEM_FAILED || errno != 0)
	{
		print_usage(strerror(errno));
		exit(EXIT_FAILURE);
	}
	return sem;
}

int main ()
{
	
}



/* eof */
