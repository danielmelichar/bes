/**
 * @file common.h
 * 
 * Beispiel 2
 * 
 * @author Daniel Melichar <ic18b503@fhtw>
 * 
 * @date 2019
 * 
 */
#ifndef COMMON_H
#define COMMON_H

/**
 * global variable that stores the application name
 */
extern char * appn;

/**
 * @brief prints the correct usage of the application
 */
void print_usage(void);

/**
 * @brief parses the command line arguments
 * @details sets the application name and parses the command line arguments
 * for the buffer size with the help of getopt().
 * 
 * @param argc number of command line arguments
 * @param argv array of strings containing the command line arguments
 * 
 * @return the buffer size specified in the command line argument
 */
int getBufferSize(int argc, char * const * argv);

#endif