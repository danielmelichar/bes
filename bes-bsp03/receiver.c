/**
 * @file receiver.c
 * Betriebsysteme Beispiel 3
 *
 * @author Daniel Melichar (ic18b503@technikum-wien.at)
 * @author Marco Judt (ic18b039@technikum-wien.at)
 * @author Florian Dienesch (ic18b509@technikum-wien.at)
 *
 * @git http://code.hc.melichar.xyz/fhtw/bes-bsp03.git
 * @date 2019
 * @version 1.0
 */

/*
 * -------------------------------------------------------------- includes --
 */

#include "sharedmem.h"

/*
 * --------------------------------------------------------------- globals --
 */

/*
 * --------------------------------------------------------------- defines--
 */

/*
 * ------------------------------------------------------------- functions --
 */

int main (int argc, char * const * argv)
{
	program_name = argv[0];
	int buffer_size = 0;
	int c;
	
    if (argc <= 1)
    {
        print_usage(strerror(errno));
        exit(EXIT_FAILURE);
    }

    while ((c = getopt(argc, argv, "m:")) != -1)
    {
    	switch (c) 
    	{    		
    		case 'm' :
    			
    			char *ptr;
    			errno = 0;
    			buffer_size = strtol (optarg, &ptr, 10);
    			
                if ((errno == ERANGE) || (errno  != 0))
                {
                    print_usage(strerror(errno));
                    exit(EXIT_FAILURE);
                }
                if (*ptr != '\0')
                {
                    print_usage(strerror(errno));
                    exit(EXIT_FAILURE);
                }
                if (buffer_size <= 0 || buffer_size >= INT_MAX)
                {
                    print_usage(strerror(errno));
                    exit(EXIT_FAILURE);
                }
                break;                     
    		
    		case ':' :
    		case '?' :
    	        print_usage(strerror(errno));
    	        exit(EXIT_FAILURE);
    			break;
    	}
    }
    
    if (optind < argc)
	{
        print_usage(strerror(errno));
        exit(EXIT_FAILURE);
	}

    ringbuffer ringbuffer = {-1, -1};
    ringbuffer->sem_read = create_semaphores(SEM_READ);
    ringbuffer->sem_write = create_semaphores(SEM_WRITE);

    do
    {
    	input = fgetc(stdin);
        if (ferror(stdin))
        {
        	print_usage(strerror(errno));
        	//CLEANUP
            return (EXIT_FAILURE);
        }

    } while (input != EOF);

    return EXIT_SUCCESS;
}

/* eof */
