/**
 * @file myfind.c
 *
 * SS-2019 - BES
 *
 * Beispiel 1 - my-find
 *
 * @author Alexandra Brugger <ic17b005@technikum-wien.at>
 * @author Illona Pidluzhna <ic18b045@technikum-wien.at>
 * @author Balint Taschner <ic18b068@technikum-wien.at>
 * @date 2019_04_17
 *
 * @version 1.0
 */

/* FB: test-find.sh:
Successful Tests: 167
Failed     Tests: 1633
Total      Tests: 1800 / 1800
*/

/* -- Includes -- */
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <fnmatch.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>


/* -- Defines -- */
#define UID_GID_BUFFER_SIZE   11   /* 2^32+1 - 4 Byte + '\0' */
#define FILE_SIZE_BUFFER_SIZE  9   /* Max file length buffer size + '\0' */

/* -- Global vars -- */
static char *prog_name;

/* -- Prototypes -- */
static void do_dir(const char *dirName, const char * const *parameters);
static void do_file(const char *file_name, const char *const *parameters);
static int param_ls(const char *file_name, const struct stat *file_info);
static const char *basename(const char *filename);
static int string_to_id(const char *id_str);
static int check_if_usr(const struct stat *file_info, const char *usr);
static int check_if_no_usr(const struct stat *file_info);
static int check_if_name(const char *file, const char *pattern);
static int check_if_path(const char *path, const char *pattern);
static int check_if_type(const struct stat *file_info, const char *flag);

/**
 *\name main
 *
 *\brief main function
 *
 *\param argc An integer that contains the number of parameters.
 *\param argv A const char pointer to command arguments.
 *
 *\return Returns EXIT_SUCCESS if successfully; otherwise EXIT_FAILURE.
 *\retval EXIT_SUCCESS
 *\retval EXIT_FAILURE
 */
/*
 * ### FB_TMG: Wenn Sie den Parameternamen einfach weglassen, dann bekommen
 * Sie diese Warnung nicht
 *
 * myfind.c error: unused parameter ‘argc’ [-Werror=unused-parameter]
 */
int main(int argc, const char *argv[])
{
    // ## FB - MD: Compile fehler: argc wird nicht verwendet (log level warn)
    // ## FB - MD: Nach compile ist myfind executable nicht ausführbar

    // ## FB - MD: Direktes allokieren von speichern ohne überprüfung von argv könnte sehr
    // schnell zu einem Bufferoverflow führen
    //Program name as global
/*
 * ### FB_TMG: Warum allozieren Sie hier Speicher? argv bleibt bis zum
 * Programmende erhalten. - Ausserdem ist sizeof(argv[0]) == 4 bzw. 8
 * weil das ein char * ist. => Das strcpy() wird furchtbar abrauchen .. [-1]
 *
 * ./myfind . -user tom
 * *** buffer overflow detected ***: ./myfind terminated
 * Aborted (core dumped)
 *
 * progname = argv[0] haette vollkommen gereicht ...
 */
    prog_name = malloc(sizeof(argv[0]));
    // ## FB - JM + DF: Fehlercheck bei malloc fehlt
    // ## FB - DF: Errorhandling für malloc fehlt. ZB Wrapper fuer malloc mit check auf NULL
    strcpy(prog_name, argv[0]);

    // ## FB - JM: argv[1] sollte vorher überprüft werden ob es überhaupt existiert ### FB_TMG: Gut [+2]
    // ## FB - JM: Wird das Programm ohne Argumente ausgeführt -> Segmentation Fault
    //Char Array Path with the lengh of the Path
    char path[strlen(argv[1])];
    //Copy Path from arguments into Path
    strcpy(path, argv[1]);

/*
 * ### FB_TMG: Und wer macht das free(), wenn in do_file()
 * ein exit() gemacht wird? [-1]
 */
    do_file(argv[1], argv);

    free(prog_name);
/*
 * ### FB_TMG: fflush(stdout) inkl. Fehlerbehandlung fehlt
 */
    //Exit Program
    return 0;
}

/**
 *\name do_dir
 *
 *\brief This functions opens a directory and iterates through.
 *
 * Works recursive with do_file().
 *
 *\param dir_name A char pointer that contains the directory name.
 *\param argv A const char pointer that contains the argument parameters.
 *
 *\return Returns no value.
 */
static void do_dir(const char *dir_name, const char * const *argv)
{
    //open directory
    DIR *currentDir = opendir(dir_name);
    //error handling if there is any
    if (currentDir == NULL) {
        fprintf(stderr, "ERROR: couldn't open %s - %s\n", dir_name, prog_name);
        return;
    }
/*
 * ### FB_TMG: errno sollten Sie nur ansehen, wenn vorher eine Libraryfunktion oder
 * ein Syscall schiefgegangen ist. - Ansonsten kann errno den Fehlerwert einer
 * anderen Libraryfunktion oder eines anderen Syscalls beinhalten
 */
    if (errno != 0) {
        fprintf(stderr, "ERROR: %s: %s - %s\n", prog_name, strerror(errno), dir_name);
        errno = 0;
    }
    //
    struct dirent *directoryEntry;

/*
 * ### FB_TMG: Hier sollten Sie errno auf 0 setzen ...
 */    
    while ((directoryEntry = readdir(currentDir))) {
        //. and .. is not needed to check
        if ((strcmp(directoryEntry->d_name, ".") == 0) || (strcmp(directoryEntry->d_name, "..") == 0))
            continue;
        //newFile with the length of dirname direntry and / and \0
        char newFile[(strlen(dir_name)) + (strlen(directoryEntry->d_name)) + 2];
        //newFile starts with path before dir_name
/*
 * ### FB_TMG: sprintf() waere einfacher hier ...
 */
        strcpy(newFile, dir_name);
        //dirname at end / if true then no / needed
        if (dir_name[strlen(dir_name) - 1] != '/')//testing if length is enough or -1 is needed?
            strcat(newFile, "/");
        //after path we add filename
        strcat(newFile, directoryEntry->d_name);
        //handle file with parameters
/*
 * ### FB_TMG: Und wer ruft closedir() auf, wenn in do_file() ein exit()
 * gemacht wird? [-1]
 */
        do_file(newFile, argv);

/*
 * ### FB_TMG: Hier sollten Sie errno auf 0 setzen ...
 */    
    }
    //handle errors if there are any
    if (errno != 0) {
        fprintf(stderr, "ERROR: %s: %s - %s\n", prog_name, strerror(errno), dir_name);
        errno = 0;
    }
    //we opend dir so we have to close it
/*
 * ### FB_TMG: Wenn Sie den Returnwert von closedir() nicht
 * verwenden (was in diesem Fall OK ist), sollten Sie den Returnwert
 * auf (void) casten, also
 *
 * (void) closedir(pDirectory);
 */    
    closedir(currentDir);
}
/**
 *\name do_file
 *
 *\brief Gets the file properties and resolve the program parameters. Recursive usage with do_dir()
 *
 *\param file_name A const char pointer that contains the file name or path.
 *\param parameters A const char pointer that contains the argument parameters.
 *
 *\return void Returns no value.
 */
static void do_file(const char *file_name, const char *const *parameters)
{

    struct stat currentEntry;
    //do lstat on file name and save it as current entry
    int lstatChecker = lstat(file_name, &currentEntry);
    //error handling
/*
 * ### FB_TMG: errno sollten Sie nur ansehen, wenn vorher eine Libraryfunktion oder
 * ein Syscall schiefgegangen ist. - Ansonsten kann errno den Fehlerwert einer
 * anderen Libraryfunktion oder eines anderen Syscalls beinhalten
 */
    if (errno != 0) {
        fprintf(stderr, "ERROR: %s: %s - %s\n", prog_name, strerror(errno), file_name);
        errno = 0;
    }
    if (lstatChecker == -1) {
        fprintf(stderr, "Error: lstat went wrong at %s\n", file_name);//prog name, errno grund, file_name
        return;
    }
    // FB - MD: im rahmen dieser Aufgabe komplett OK, bei zukünftigen Aufgaben wärs nett wenn ich mir
    // die stelle, an welcher ich die Argumente und Werte angebe, selber aussuchen kann
    //starts at 2, because 0 = progname 1 = path
    /* FB - JM: Dann müsste currentParam auch 2 sein; so wird die while-Schleife bei einer Aktion
     * immer übersprungen (z.B. bei ~/ -print)
     * ... und bei mehr Argumenten wird falsch überprüft    */
/*
 * ### FB_TMG: Und warum setzten Sie das dann auf 3?
 */
    int currentParam = 3;

    int output_flag = 1; /* 0.. already printed; 1.. no print */
    int filter_flag = 0; /* 0.. no filter;       1.. filter   */
    //while is true until no paramters left
    while (parameters[currentParam] != NULL) {

        // # FB - MD: performance ist meh, es benötigt 6 vergleiche
        // bis die logik von myfind . -ls grefit
        if (strcmp(parameters[currentParam], "-user") == 0) {

            if (!parameters[currentParam + 1]) {                    /* check if second parameter is missing */
                // ## FB - JM: Fehlermeldungen sollten den Programmnamen ausgeben ### FB_TMG: Gut [+2]
                fprintf(stderr, "Error: missing argument to %s", parameters[currentParam]);
                exit(EXIT_FAILURE);
            }

            if (!filter_flag) { /* of filter already enabled do not check file name again*/

                filter_flag = check_if_usr(&currentEntry, parameters[++currentParam]);
            } else {

                ++currentParam;
            }
        } else if (strcmp(parameters[currentParam], "-nouser") == 0) {
            if (!filter_flag) { /* of filter already enabled do not check file name again*/

                filter_flag = !check_if_no_usr(&currentEntry);
            }
        } else if (strcmp(parameters[currentParam], "-name") == 0) {

            if (!parameters[currentParam + 1]) {                    /* check if second parameter is missing */
                // ## FB - JM: Fehlermeldungen sollten den Programmnamen ausgeben
                fprintf(stderr, "Error: missing argument to %s", parameters[currentParam]);
                exit(EXIT_FAILURE);
            }

            if (!filter_flag) { /* of filter already enabled do not check file name again*/

                filter_flag = check_if_name(file_name, parameters[currentParam++]);
            } else {
                // # FB - MD: wieso ist das die einzige Zeile wo c++ anstatt ++c verwendet wird?
                currentParam++;
            }
        } else if (strcmp(parameters[currentParam], "-path") == 0) {

            if (!parameters[currentParam + 1]) {                    /* check if second parameter is missing */
                // ## FB - JM: Fehlermeldungen sollten den Programmnamen ausgeben
                fprintf(stderr, "Error: missing argument to %s", parameters[currentParam]);
                exit(EXIT_FAILURE);
            }

            if (!filter_flag) { /* of filter already enabled do not check file name again*/

                filter_flag = check_if_path(file_name, parameters[++currentParam]);
            } else {

                ++currentParam;
            }
        } else if (strcmp(parameters[currentParam], "-type") == 0) {

            if (!parameters[currentParam + 1]) {                    /* Error handling. Second parameter is missing */
                // ## FB - JM: Fehlermeldungen sollten den Programmnamen ausgeben
                fprintf(stderr, "Error: missing argument to %s", parameters[currentParam]);
                exit(EXIT_FAILURE);
            }

            if (strlen(parameters[currentParam + 1]) != 1) {        /* Error handling. Second parameter contains too many options */
                // ## FB - JM: Fehlermeldungen sollten den Programmnamen ausgeben
                fprintf(stderr, "Error: Arguments to -type should contain only one letter");
                exit(EXIT_FAILURE);
            }

/*
 * ### FB_TMG: Das haette ich in check_if_type() gemacht, weil so machen Sie das
 * doppelt ...
 */
            if (*parameters[currentParam + 1] != 'b' &&
                *parameters[currentParam + 1] != 'c' &&
                *parameters[currentParam + 1] != 'd' &&
                *parameters[currentParam + 1] != 'p' &&
                *parameters[currentParam + 1] != 'l' &&
                *parameters[currentParam + 1] != 'f' &&
                *parameters[currentParam + 1] != 's') {           /* Error handling. Second parameter contains unknown argument */
                // ## FB - JM: Fehlermeldungen sollten den Programmnamen ausgeben
                fprintf(stderr, "Error: Unknown argument to -type");
                exit(EXIT_FAILURE);
            }

            if (!filter_flag) { /* of filter already enabled do not check file name again*/

                filter_flag = check_if_type(&currentEntry, parameters[++currentParam]);
            } else {

                ++currentParam;
            }
        } else if (strcmp(parameters[currentParam], "-ls") == 0) {

            // ## FB - JM: Ich würde so einen Ternary Operator nicht als Parameter übergeben
/*
 * ### FB_TMG: Warum uebergeben Sie da einen NULL pointer wenn filter_flag 1 ist?
 * param_ls() kann nicht damit umgehen, dass es file_name == NULL bekommt [-1]
 */
            output_flag = param_ls(!filter_flag ? file_name : NULL, &currentEntry);
        } else if (strcmp(parameters[currentParam], "-print") == 0) {
/*
 * ### FB_TMG: Fehlerbehandlung printf() fehlt
 */
            printf("%s\n", file_name);
/*
 * ### FB_TMG: errno sollten Sie nur ansehen, wenn vorher eine Libraryfunktion oder
 * ein Syscall schiefgegangen ist. - Ansonsten kann errno den Fehlerwert einer
 * anderen Libraryfunktion oder eines anderen Syscalls beinhalten
 */
            if (errno != 0) {
                fprintf(stderr, "ERROR: %s: %s - %s\n", prog_name, strerror(errno), file_name);
                errno = 0;
            }
        } else {
/*
 * ### FB_TMG: Das sollte wohl parameters[currentParam] heissen ...
 */
            fprintf(stderr, "Error: %s: unknown parameter \"%s\" \n", prog_name, parameters[1]);
            exit(EXIT_FAILURE);
        }
        currentParam++;
    }

    /* if no print or ls parameter is set (output_flag != 0) */
/*
 * ### FB_TMG: print als default action funktioniert nicht korrekt [-1]
 *
 * -print set nicht output_flag auf 0
 */
    if (output_flag)
/*
 * ### FB_TMG: Fehlerbehandlung printf() fehlt [-1]
 */
        printf("%s\n", file_name);

    if (S_ISDIR(currentEntry.st_mode))
        do_dir(file_name, parameters);
}
/**
 *\name param_ls
 *
 *\brief Used for parameter -ls. Shows filename details.
 *
 *\param file_name The filename to print
 *\param currentEntry stat struct of the current entry
 *
 *\return Returns an integer that contains EXIT_SUCCESS if no error occurred; otherwise EXIT_FAILURE.
 *\retval 0 EXIT_SUCCESS - No error occurred.
 *\retval 1 EXIT_FAILURE - An error occurred
 */
static int param_ls(const char *file_name, const struct stat *currentEntry)
{
    struct passwd *usr = getpwuid(currentEntry->st_uid);   /* get password info */
    // ## FB - JM: Würde hier auch noch auf usr == NULL checken
/*
 * ### FB_TMG: errno sollten Sie nur ansehen, wenn vorher eine Libraryfunktion oder
 * ein Syscall schiefgegangen ist. - Ansonsten kann errno den Fehlerwert einer
 * anderen Libraryfunktion oder eines anderen Syscalls beinhalten
 */
/*
 * ### FB_TMG: Hmmmm ... - ENOENT bzw. ESRCH ... weisen aber auch darauf
 * hin, dass der User nicht gefunden wurde [-1]
 *
 * ERRORS
 *  0 or ENOENT or ESRCH or EBADF or EPERM or ...
 *         The given name or uid was not found.
 */
/*
 * ### FB_TMG: Wenn es keinen user Eintrag gibt, dann soll die user ID
 * ausgegeben werden [-1]
 */
    if (errno != 0) {
        fprintf(stderr, "ERROR: %s: %s - %s\n", prog_name, strerror(errno), file_name);
        errno = 0;
        exit(EXIT_FAILURE);
    }
    struct group  *grp = getgrgid(currentEntry->st_gid);   /* get group info    */
    // ## FB - JM: Würde hier auch noch auf grp == NULL checken
/*
 * ### FB_TMG: errno sollten Sie nur ansehen, wenn vorher eine Libraryfunktion oder
 * ein Syscall schiefgegangen ist. - Ansonsten kann errno den Fehlerwert einer
 * anderen Libraryfunktion oder eines anderen Syscalls beinhalten
 */
/*
 * ### FB_TMG: Hmmmm ... - ENOENT bzw. ESRCH ... weisen aber auch darauf
 * hin, dass die Gruppe nicht gefunden wurde
 *
 * ERRORS
 *  0 or ENOENT or ESRCH or EBADF or EPERM or ...
 *         The given name or gid was not found.
 */
/*
 * ### FB_TMG: Wenn es keinen user Eintrag gibt, dann soll die group ID
 * ausgegeben werden [-1]
 */
    if (errno != 0) {
        fprintf(stderr, "ERROR: %s: %s - %s\n", prog_name, strerror(errno), file_name);
        errno = 0;
        exit(EXIT_FAILURE);
    }
    /* Decode file type */
    char file_type = '-';                               /* init file type    */

    switch (currentEntry->st_mode & S_IFMT) {

        case S_IFSOCK:
            file_type = 's';  /* socket     */
            break;
        case S_IFLNK:
            file_type = 'l';  /* link       */
            break;
        case S_IFREG:
            file_type = '-';  /* regular    */
            break;
        case S_IFBLK:
            file_type = 'b';  /* block      */
            break;
        case S_IFDIR:
            file_type = 'd';  /* directory  */
            break;
        case S_IFCHR:
            file_type = 'c';  /* character  */
            break;
        case S_IFIFO:
            file_type = 'p';  /* FIFO       */
            break;
        default:
            file_type = '-';  /* unknown    */
    }

    /* Decode uid */
    char uid[UID_GID_BUFFER_SIZE];
    if (sprintf(uid, "%u", currentEntry->st_uid) < 0) {

        fprintf(stderr, "ERROR: %s: An error occurred while set uid - %s\n", prog_name, file_name);
        exit(EXIT_FAILURE);
    }

    /* Decode gid */
    char gid[UID_GID_BUFFER_SIZE];
    if (sprintf(gid, "%u", currentEntry->st_gid) < 0) {
        fprintf(stderr, "ERROR: %s: An error occurred while set gid - %s\n", prog_name, file_name);
        exit(EXIT_FAILURE);
    }

    /* Decode owner exec bit          */
    char own_exec = (currentEntry->st_mode & S_IXUSR) ?
                    ((currentEntry->st_mode & S_ISUID) ? 's' : 'x') :
                    ((currentEntry->st_mode & S_ISUID) ? 'S' : '-');

    /* Decode group exec bit          */
    char grp_exec = (currentEntry->st_mode & S_IXGRP) ?
                    ((currentEntry->st_mode & S_ISGID) ? 's' : 'x') :
                    ((currentEntry->st_mode & S_ISGID) ? 'S' : '-');

    /* Decode other exec (sticky) bit */
    char oth_exec = (currentEntry->st_mode & S_IXGRP) ?
                    ((currentEntry->st_mode & S_ISVTX) ? 't' : 'x') :
                    ((currentEntry->st_mode & S_ISVTX) ? 'T' : '-');

    // # FB - MD : ohne nachzuschauen, das wirds sicher auch in der stdlib drinnen sein
    /* Decode file time               */
    char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    struct tm *date_time = localtime(&currentEntry->st_mtime);

    if (date_time == NULL) {

        fprintf(stderr, "ERROR: %s: An error occurred while calcutating date_time - %s\n", prog_name, file_name);
        exit(EXIT_FAILURE);
    }

    /* Decode file size */ /* if file type is block or char don't show file length */
    char file_size[FILE_SIZE_BUFFER_SIZE];

/*
 * ### FB_TMG: Size bei block und character special files wird korrekt
 * behandelt
 */
    if (currentEntry->st_size == 0 && (file_type == 'c' || file_type == 'b')) {

        file_size[0] = '\0';

    } else {

        if (sprintf(file_size, "%lld", (long long)currentEntry->st_size) < 0) {

            fprintf(stderr, "ERROR: %s: An error occurred while set uid - %s\n", prog_name, file_name);
            exit(EXIT_FAILURE);
        }
    }

    /* ### FB: GRP14: number of links wird hier als int ausgegeben (14.Argument) -> m\FCsste eigentlich long unsigned int sein  */
    /* Print file details */
    if (printf("%6ld %4d %c%c%c%c%c%c%c%c%c%c %3d %-8s %-8s %-8s %3s %2d %02d:%02d %s%s",
               currentEntry->st_ino,

            // ## FB - JM: Denke man sollte hier auch irgendwie getenv Fehlerchecken ### FB_TMG: Nope				      /* the index number of file      */
/*
 * ### FB_TMG: POSIXLY_CORRECT wird korrekt behandelt
 */
/*
 * ### FB_TMG: Die Referenzimplementierung gibt bei symlinks 0 als
 * Anzahl der blocks aus
 */
               getenv("POSIXLY_CORRECT") ? (int)currentEntry->st_blocks : ((int)currentEntry->st_blocks + 1) / 2,  /* blocks for disk usage         */
               file_type,                                                                                    /* file type bit                 */
               (currentEntry->st_mode & S_IRUSR) ? 'r' : '-',                                                   /* owner read allowed bit        */
               (currentEntry->st_mode & S_IWUSR) ? 'w' : '-',                                                   /* owner write allowed bit       */
               own_exec,                                                                                     /* owner exec bit (SUID)         */
               (currentEntry->st_mode & S_IRGRP) ? 'r' : '-',                                                   /* group read allowed bit        */
               (currentEntry->st_mode & S_IWGRP) ? 'w' : '-',                                                   /* group write allowed bit       */
               grp_exec,                                                                                     /* group exec bit (SGID)         */
               (currentEntry->st_mode & S_IROTH) ? 'r' : '-',                                                   /* others read allowed bit       */
               (currentEntry->st_mode & S_IWOTH) ? 'w' : '-',                                                   /* others write allowed bit      */
               oth_exec,                                                                                     /* others exec (sticky) bit      */
/*
 * ### FB_TMG: Besser cast auf unsigned long (oder size_t)
 */
               (int)currentEntry->st_nlink,                                                                          /* number of links               */
               usr ? usr->pw_name : uid,                                                                     /* user name                     */
               grp ? grp->gr_name : gid,                                                                     /* group name                    */
/*
 * ### FB_TMG: file_size tut's auch
 */
               &file_size[0],                                                                                /* file size                     */
/*
 * ### FB_TMG: strftime() ist Ihr Freund ...
 */
               months[date_time->tm_mon],                                                                    /* date and time [month] of file */
               date_time->tm_mday,                                                                           /* date and time [day] of file   */
               date_time->tm_hour,                                                                           /* date and time [hour] of file  */
               date_time->tm_min,                                                                            /* date and time [min] of file   */
               file_name,                                                                                    /* filename                      */
               S_ISLNK(currentEntry->st_mode) ? "" : "\n")                                                      /* link path                     */
        < 0) {   /* Error handling - Error handling - on error -> -1
                                                errno: No error number */

        fprintf(stderr, "ERROR: %s: An error occurred while print filename details - %s\n", prog_name, file_name);
        exit(EXIT_FAILURE);
    }

    /* Decode link path */
    if (S_ISLNK(currentEntry->st_mode)) {

        // ## FB - JM: Wieso in einer Zeile?
        char link[currentEntry->st_size + 5]; strcpy(link, " -> "); link[currentEntry->st_size + 4] = '\0';         /* " -> " + buffer + '\0'       */

        // # FB - MD: wieso ist die formattierung hier so komisch? 
        errno = 0; /* Init. errno */
/*
 * ### FB_TMG: Hier luegen Sie readlink() an, weil Sie sagen, dass Ihr
 * Buffer 4 bytes laenger ist, als der Buffer, den Sie ihm geben
 */
        if (readlink(file_name, &link[4], sizeof(link)) < 0) { /* Error handling - Error handling - on error -> -1 and errno is set
                                                                                               errno: EACCES  - Search permission is denied for a component of the path prefix.
                                                                                                      EFAULT  - buf extends outside the process\92s allocated address space.
                                                                                                      EINVAL  - bufsiz is not positive.
                                                                                                      EINVAL  - The named file is not a symbolic link.
                                                                                                      EIO     - An I/O error occurred while reading from the file system.
                                                                                                      ELOOP   - Too many symbolic links were encountered in translating the pathname.
                                                                                                      ENAMETOOLONG - A pathname, or a component of a pathname, was too long.
                                                                                                      ENOENT  - The named file does not exist.
                                                                                                      ENOMEM  - Insufficient kernel memory was available.
                                                                                                      ENOTDIR - A component of the path prefix is not a directory. */

            fprintf(stderr, "ERROR: %s: An error occurred while readlink\n", prog_name);
            exit(EXIT_FAILURE);
        }

        /* print link */
/*
 * ### FB_TMG: link tut's auch
 */
        if (printf("%s\n", &link[0]) < 0) { /* Error handling - Error handling - on error -> -1
                                                                            errno: No error number */

            fprintf(stderr, "ERROR: %s: An error occurred while print file link details\n", prog_name);
            exit(EXIT_FAILURE);
        }
    }
    return 1;
}
/**
 *\name check_if_usr
 *
 *\brief checks if user is the fileowner of a file
 *
 *
 *
 *\param currentEntry A pointer to a struct stat of the file to check
 *\param usr A char pointer to a string that contains the username or uid.
 *
 *
 *\return int
 *        Returns 0 if user is fileowner
 *        Returns 1 if user is not fileowner
 */
static int check_if_usr(const struct stat *currentEntry, const char *usr)
{
    const struct passwd *pwd_entry;
    int uid;

    errno = 0;
    pwd_entry = getpwnam(usr);

    if (pwd_entry == NULL) {
/*
 * ### FB_TMG: Fehlerbehandlung getpwnam() fehlt - Hier müssen Sie die
 * errno ansehen ... [-1]
 */
        /*check if usr is a valid uid */
        uid = string_to_id(usr);
        if (uid == -1) {
            fprintf(stderr, "%s: `%s' is not the name of a known user\n", prog_name, usr);
            exit(1);
        }

        if (uid == -2) {
            fprintf(stderr, "%s: %s: Numerical result out of range\n", prog_name, usr);
            exit(1);
        }

        errno = 0;
/*
 * ### FB_TMG: Warum checken Sie hier via getpwuid() ob die UID in
 * /etc/passwd vorhanden ist? - Damit kann man nur nach directory
 * entries suchen, die einem user gehören, der in /etc/passwd steht
 * ... [-1]
 *
 * einfach uid mit currentEntry->st_uid vergleichen ...
 */
        pwd_entry = getpwuid(uid);
/*
 * ### FB_TMG: Fehlerbehandlung getpwuid() fehlt - Hier müssen Sie die
 * errno ansehen ... [-1]
 */
        if (pwd_entry == NULL) {
            fprintf(stderr, "%s: `%s' is not the name of a known user\n", prog_name, usr);
            exit(1);
        }
    }

    if (pwd_entry->pw_uid == currentEntry->st_uid) { //if usr is fileowner
        return 0;
    }

    //if fileowner not usr
    return 1;
}
/**
 *\name string_to_id
 *
 *\brief converts a uid or gid given as string to an integer id
 *
 *
 *
 *\param id_str A char pointer that contains the directory name.
 *
 *
 *\return int
 *        Returns an id
 *        Returns -1 if id_str not only contains numbers
 *				Returns -2 if id_str is larger than 2147483647
 */
static int string_to_id(const char *id_str)
{

    long long id = 0;
    char *endptr = NULL;

    id = strtoll(id_str, &endptr, 10);

/*
 * ### FB_TMG: Fehlerbehandlung strtoll() unvollstaending => manpage ansehen!
 */
    /*check if in id_str are other chars than numbers*/
    if (endptr != (id_str + strlen(id_str))) {
        return -1;
    }

    // ## FB - DF: wo ist die variable nbr?
    // ## FB - DF: check auf maximaler int wer koennte mit der variable "INT_MAX" gemacht werden, die befin
/*
 * ### FB_TMG: Konstanten aus limits.h verwenden ...
 */
    /*check if nbr  < 2147483647 */
    if (id > 2147483647) {
        return -2;
    }
    return id;
}
/**
 *\name check_if_name
 *
 *\brief The check_if_name() function checks whether the string argument matches the pattern argument
 *
 *
 *\param file A char pointer to a string that contains the file name.
 *\param pattern A char pointer to a string that contains the pattern argument.
 *
 *\return Returns an integer that contains the value EXIT_SUCCESS if found; otherwise EXIT_FAILURE.
 *\retval EXIT_SUCCESS
 *\retval EXIT_FAILURE
 */
static int check_if_name(const char *file, const char *pattern)
{


/*
 * ### FB_TMG: Verwendung von FNM_NOESCAPE führt zu Differenzen zu
 * normalem find bei Files die '\' enthalten und '\' im pattern haben [-1]
 */
    int match = fnmatch(pattern, basename(file), FNM_NOESCAPE);

    if (match != FNM_NOMATCH && match != 0) {     /* Error handling - Error handling - on error -> match != FNM_NOMATCH && match != 0
                                                                    errno: Not used */

        fprintf(stderr, "ERROR: %s: An error occurred while match file name - %s\n", prog_name, file);
        exit(EXIT_FAILURE);
    }

    return (!match ? EXIT_SUCCESS : EXIT_FAILURE);
}
/**
 *\name basename
 *
 *\brief Returns an const char pointer that contains the base name.
 *
 *\param filename A char pointer that contains the full file name.
 *
 *\return Returns an const char pointer that contains the base name.
 */
static const char *basename(const char *filename)
{


    char *base_name = strrchr(filename, '/');

    return !base_name ? filename : ++base_name;
}
/**
 *\name check_if_path
 *
 *\brief The check_if_path() function checks whether the string argument matches the pattern argument
 *
 *\param file A char pointer to a string that contains the path name.
 *\param pattern A char pointer to a string that contains the pattern argument.
 *
 *\return Returns an integer that contains the value EXIT_SUCCESS if found; otherwise EXIT_FAILURE.
 *\retval EXIT_SUCCESS
 *\retval EXIT_FAILURE
 */
static int check_if_path(const char *path, const char *pattern)
{

/*
 * ### FB_TMG: Verwendung von FNM_NOESCAPE führt zu Differenzen zu
 * normalem find bei Files die '\' enthalten und '\' im pattern haben
 */
    int match = fnmatch(pattern, path, FNM_NOESCAPE);

    if (match != FNM_NOMATCH && match != 0) {     /* Error handling - Error handling - on error -> match != FNM_NOMATCH && match != 0
                                                                    errno: Not used */

        fprintf(stderr, "ERROR: %s: An error occurred while match path\n", prog_name);
        exit(EXIT_FAILURE);
    }

    return (!match ? EXIT_SUCCESS : EXIT_FAILURE);
}
/**
 *\name check_if_type
 *
 *\brief Compares file name properties.
 *
 *
 *\param file_info A struct pointer to the file properties.
 *\param flag A const char pointer that contains the property flag.
 *
 *\return Return EXIT_SUCCESS (0) if flag is set; otherwise EXIT_FAILURE (1) is set.
 *\retval EXIT_SUCCESS (0)
 *\retval EXIT FAILURE (1)
 */
static int check_if_type(const struct stat *file_info, const char *flag)
{

/*
 * ### FB_TMG: S_ISDIR(), S_ISREG(), ... sind Ihre Freunde ...
 */
    switch (file_info->st_mode & S_IFMT) {  /* Mask file mode */
        case S_IFBLK:
            return !(*flag == 'b');             /* Block special file */
        case S_IFCHR:
            return !(*flag == 'c');             /* Character special file */
        case S_IFDIR:
            return !(*flag == 'd');	            /* Directory */
        case S_IFIFO:
            return !(*flag == 'p'); 	          /* Pipe */
        case S_IFREG:
            return !(*flag == 'f');	            /* Reg. file name */
        case S_IFLNK:
            return !(*flag == 'l');             /* Symbolic link */
        case S_IFSOCK:
            return !(*flag == 's');	            /* Socket */
        default:
            return EXIT_FAILURE;
    }
}
/**
 *\name check_if_no_usr
 *
 *\brief checks if file belongs to no valid user
 *
 *\param  file_info A pointer to a struct stat of the file to check
 *
 *
 *\return int
 *        Returns 0 if user of the file exists in passwd
 *        Returns 1 if user of the file does not exist
 */
static int check_if_no_usr(const struct stat *file_info)
{

    errno = 0;
    if (getpwuid(file_info->st_uid) == NULL) {
/*
 * ### FB_TMG: Hmmmm ... - ENOENT bzw. ESRCH ... weisen aber auch darauf
 * hin, dass der User nicht gefunden wurde
 *
 * ERRORS
 *  0 or ENOENT or ESRCH or EBADF or EPERM or ...
 *         The given name or uid was not found.
 */
        if (errno != 0) {
            fprintf(stderr, "ERROR: %s: %s\n", prog_name, strerror(errno));
            errno = 0;
        }
        return 1;                 // falls ung\FCltiger user
    }

    return 0;
}
